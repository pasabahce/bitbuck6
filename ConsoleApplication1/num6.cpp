#include "stdafx.h"


using namespace System;

using namespace System::Collections;

int main()

{
	String^ str;
	String^ nl = Environment::NewLine;
	Console::WriteLine();
	Console::WriteLine("-- Environment members --");
	Console::WriteLine("CommandLine: {0}", Environment::CommandLine);
	array<String^>^arguments = Environment::GetCommandLineArgs();
	Console::WriteLine("GetCommandLineArgs: {0}", String::Join(", ", arguments));
	Console::WriteLine("SystemDirectory: {0}", Environment::SystemDirectory);
	Console::WriteLine("UserName: {0}", Environment::UserName);
	Console::WriteLine("GetFolderPath: {0}", Environment::GetFolderPath(Environment::SpecialFolder::System));

	array<String^>^drives = Environment::GetLogicalDrives();
	Console::WriteLine("GetLogicalDrives: {0}", String::Join(", ", drives));
}